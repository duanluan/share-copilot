The beta version now supports full interface proxy, 

So even code suggestions go through the proxy, 

Still in testing.



![输入图片说明](readme/screenshots.gif)
***
# share-copilot 

- Acts as a proxy server, forwarding requests to the Copilot plugin's API

- Supports both vscode and Jetbrains plugins

- Supports multiple users sharing a single token

- Optimize request logic to reduce suspended probability、

  

![软件系统网络架构.png](https://img1.imgtp.com/2023/09/10/qTL8A2u9.png)

***
# 一、Build It Yourself：
```sh
Test Environment: Linux4.18.0-305.3.1.el8.x86_64 GNU/Linux
Requires Go environment; 
resolve any issues chatgpt.
```
```sh
git clone https://gitlab.com/luyoyu/share-copilot.git
wget https://dl.google.com/go/go1.21.1.linux-amd64.tar.gz
tar -C /usr/local -xzf go1.21.1.linux-amd64.tar.gz

vim ~/.bashrc
# Add the following environment variables
export PATH=$PATH:/usr/local/go/bin
export GOPATH=$HOME/go
export PATH=$PATH:$GOPATH/bin

# Make the environment variables effective
source ~/.bashrc
# Verify the installation
go version 
# CD to the source directory of main.go
cd /share-copilot/source
go build
```
***
# 二、Use the Ready-Made Version：

### 1.Installation

```sh
wget https://gitlab.com/luyoyu/share-copilot/uploads/73bca0887b3ea39622e88cb88d7d0297/share-copilot-linux-amd64.zip
```
```sh
unzip share-copilot-linux-amd64.zip 
```

```sh
cd share-copilot
```

```sh
bash install.sh
```

### 2.Configuration

```sh
vim  config.json 
```

##### 2.1 Explanation of config.json

```js
domain // Listening domain or IP (can be reverse proxied with nginx)
host   // IP
port   // Port 80 or 443
certPath // Public key path - required for 443
keyPath  // Private key path - required for 443
github_api_url // Default, usually not changed
token  // Your Copilot token(s), you can put multiple, they will be used randomly in requests
// After logging into the Copilot plugin, find them in %userprofile%\AppData\Local\github-copilot\host.json (starting with "ghu_")
verification // Custom verification

```

##### 2.2 Example 

```json
{
	"server": {
		"domain": "example.com",
		"host": "0.0.0.0",
		"port": 443,
		"certPath":"./example.pem",
		"keyPath":"./example.pem"
	},
	"copilot_config":{
		"github_api_url": "https://api.github.com/copilot_internal/v2/token",
		"token":[
			"yours_token_1",
            "yours_token_2",
            "yours_token_3"
		]},
	"verification":""
}

```
The format must be correct;

### 3.Run 

```sh
scop r  # Run [Ctrl+c to exit]  chmod +x share-copilot if Permission denied
scop rb # Run in the background
scop st # Stop
scop v  # View status
```

### 4.Complete Example：

##### 4.1 Server-side Configuration (Modify config.json)

```json
{
	"server": {
		"domain": "api.example.com",
		"host": "0.0.0.0",
		"port": 443,
		"certPath":"./example.pem",
		"keyPath":"./example.pem"
	},
	"copilot_config":{
		"github_api_url": "https://api.github.com/copilot_internal/v2/token",
		"token":[
		"ghu_xMNAYLcJAPqAfiGoobrWffkJoNcGMVJtETKA",
        "ghu_GZgKFwraHorAxXXUvsUclOhxiYERPsSJeNuF",
        "ghu_SPUTCLvkMKoeMstPJmhSlYsYvCojhkFjGubl"
		]},
	"verification":"i_am_free"
}
```

------

##### 4.3Local Configuration Changes:

**Using Windows as an example**

- **For Jetbrains Plugin: Modify %userprofile%\AppData\Local\github-copilot\host.json**

```json
{
    "github.com":{
        "user":"suibian",//Fill in as desired
        "oauth_token":"i_am_free",// Corresponds to the "verification" value above
        "dev_override":{
            "copilot_token_url":"https://api.example.com/copilot_internal/v2/token"  
            					// Your address
        }
    }
}
```
- **For Vscode Plugin: Modify %userprofile%\.vscode\extensions\github.copilot-xxxx\dist\extension.js**

```js
//Add the following code, note that you'll need to re-add it if the vscode plugin gets updated; Jetbrains does not require this step
process.env.GITHUB_TOKEN="i_am_free"; // Corresponds to the "verification" value above
process.env.GITHUB_API_URL="https://api.example.com"; // Your address
process.env.CODESPACES="true";
process.env.GITHUB_SERVER_URL="https://github.com";
```

------

##### **Successful Testing:** 

![demo2.png](https://img1.imgtp.com/2023/09/09/FHDNLixL.png)
