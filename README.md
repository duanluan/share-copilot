[中文](README.md) |[English](README-EN.md) 

***
# share-copilot   

- 作为代理服务器，中转copilot插件的API的请求
- 支持vscode插件和jetbrains插件
- 支持多用户共享一个token
- 优化请求逻辑，降低suspended概率（别万人骑基本不会）

```sh
简单说一下
本地插件的请求逻辑：

1.本地插件携ghu_token请求github的API

2.返回带有时间戳的copilot的token(下面简称co_token)，
  代码提示都是用的co_token

3.co_token到期->ghu_token重新请求->获得新的co_token
-------------------------------------------------------------

代理服务器的逻辑:

代理的是"ghu_token请求"这一环节

1.携ghu_token请求github的API，暂存co_token

2.多用户请求时，判断co_token有没有过期，没过期直接返回，
  减少ghu_token的请求次数（大约10-20分钟过期）

3.至于本地co_token，也就是代码提示没有走代理（可以代理，但是要修改插件，jetbrains的插件还要重新build）
  猜测风控目前也只是停留在请求ghu_token这一层
  
"cocopilot也是代理了ghu_token请求，并没有破解插件"

```

![软件系统网络架构.png](https://img1.imgtp.com/2023/09/10/qTL8A2u9.png)

***
# 一、自行编译：
```
测试环境:Linux4.18.0-305.3.1.el8.x86_64 GNU/Linux
需要 go环境 出问题自行chatgpt
```
```sh
git clone https://gitee.com/chuangxxt/share-copilot.git
wget https://dl.google.com/go/go1.21.1.linux-amd64.tar.gz
tar -C /usr/local -xzf go1.21.1.linux-amd64.tar.gz

vim ~/.bashrc
#添加下列环境
export PATH=$PATH:/usr/local/go/bin
export GOPATH=$HOME/go
export PATH=$PATH:$GOPATH/bin
#使生效环境变量
source ~/.bashrc
#验证go配置
go version 

#cd到main.go源码目录
cd /share-copilot/src
go build
```
***
# 二、使用现成：

### 1.安装

```sh
wget https://gitee.com/chuangxxt/share-copilot/releases/download/1.0.2/share-copilot-linux-amd64.zip
```
```sh
unzip share-copilot-linux-amd64.zip 
```
```sh
cd share-copilot
```
```sh
bash install.sh
```

### 2.配置

```sh
vim  config.json #修改配置信息
```

##### 2.1 config.json文件说明

```js
domain//监听域名或ip   可用nginx反代

host //ip

port //端口 80或443

certPath//公钥路径-443必须

keyPath//私钥路径-443必须

github_api_url//默认不改

token//自己的副驾驶token,可放多个，请求时随机使用
//登录插件后在%userprofile%\AppData\Local\github-copilot\hosts.json     ghu_开头

verification//自定义验证
```

##### 2.2 示例 

```json
{
	"server": {
		"domain": "example.com",
		"host": "0.0.0.0",
		"port": 443,
		"certPath":"./example.pem",
		"keyPath":"./example.pem"
	},
	"copilot_config":{
		"github_api_url": "https://api.github.com/copilot_internal/v2/token",
		"token":[
			"yours_token_1",
            "yours_token_2",
            "yours_token_3"
		]},
	"verification":""
}

```
格式不能错，不确定的去这里验证 ->https://www.json.cn/

### 3.启动 

```sh
scop r  #运行 [Ctrl+c退出] 如果运行失败添加可执行权限 chmod +x share-copilot
scop rb #后台运行
scop st #停止
scop v  #查看是否运行
```

### 4.完整例子：

##### 4.1 服务端配置修改config.json

```json
{
	"server": {
		"domain": "api.example.com",
		"host": "0.0.0.0",
		"port": 443,
		"certPath":"./example.pem",
		"keyPath":"./example.pem"
	},
	"copilot_config":{
		"github_api_url": "https://api.github.com/copilot_internal/v2/token",
		"token":[
		"ghu_xMNAYLcJAPqAfiGoobrWffkJoNcGMVJtETKA",
        "ghu_GZgKFwraHorAxXXUvsUclOhxiYERPsSJeNuF",
        "ghu_SPUTCLvkMKoeMstPJmhSlYsYvCojhkFjGubl"
		]},
	"verification":"i_am_free"
}
```

##### 4.2 运行成功截图：

![yunxingceshi](readme/yunxingceshi.png)
```
Jetbrains       -- 用于Jetbrains插件
Vscode          -- 用于Vscode插件
Valid tokens:   -- 配置里面有效的token数量
Succeed:        -- 插件请求代理成功次数
Failed:         -- 插件请求代理失败次数
GithubApi:      --服务器请求gh_api次数
正常情况下 GithubApi < Succeed 因为请求的Token 几十分钟才过期
```
------

##### 4.3本地配置修改：

**以windows为例**

- **jetbrains插件修改：%userprofile%\AppData\Local\github-copilot\hosts.json**

```json
{
    "github.com":{
        "user":"suibian",//随意填写
        "oauth_token":"i_am_free",//与上面verification对应
        "dev_override":{
            "copilot_token_url":"https://api.example.com/copilot_internal/v2/token"  
            					//你的地址
        }
    }
}
```
- **vscode插件修改：%userprofile%\\.vscode\extensions\github.copilot-xxxx\dist\extension.js**

```js
//添加下列代码，注意vscode插件更新需要重新添加，jetbrains则不用
process.env.GITHUB_TOKEN="i_am_free"; //与上面verification对应
process.env.GITHUB_API_URL="https://api.example.com"; //你的地址
process.env.CODESPACES="true";
process.env.GITHUB_SERVER_URL="https://github.com";
```

------

##### **4.4测试成功：** 

![demo2.png](https://img1.imgtp.com/2023/09/09/FHDNLixL.png)
