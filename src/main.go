package main

func main() {
	// 初始化配置文件
	configFile = initConfig()

	// 创建Gin引擎
	engine := setupGinEngine()

	// 初始化有效的token列表
	initValidTokenList()

	// 定义路由
	setupRoutes(engine)

	// 初始化并启动服务器
	initAndStartServer(engine)

	// 显示信息
	showMsg()
}
